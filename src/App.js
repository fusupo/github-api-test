import axios from 'axios';
import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import SearchField from './components/SearchField';
import UserProfile from './components/UserProfile';

const numToCompare = 3;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      usernames: new Array(numToCompare).fill(''),
      successes: new Array(numToCompare).fill(null),
      errors: new Array(numToCompare).fill(null),
      searchInProgress: false
    };
  }

  _updateUsername(idx, newVal) {
    const newUsernames = this.state.usernames.slice(0);
    const newSuccesses = this.state.successes.slice(0);
    const newErrors = this.state.errors.slice(0);
    newErrors[idx] = null;
    newSuccesses[idx] = null;
    newUsernames[idx] = newVal;
    this.setState({
      usernames: newUsernames,
      successes: newSuccesses,
      errors: newErrors
    });
  }

  _doSearch() {
    this.setState({ searchInProgress: true });
    const gets = this.state.usernames.map((un, idx) => {
      const username = this.state.usernames[idx];
      const url = `https://api.github.com/users/${username}/repos`;
      return axios
        .get(url)
        .then(
          function(response) {
            const newSuccesses = this.state.successes.slice(0);
            const newErrors = this.state.errors.slice(0);
            newSuccesses[idx] = response.data;
            newErrors[idx] = null;
            this.setState({
              successes: newSuccesses,
              errors: newErrors
            });
          }.bind(this)
        )
        .catch(
          function(response) {
            const newSuccesses = this.state.successes.slice(0);
            const newErrors = this.state.errors.slice(0);
            newSuccesses[idx] = null;
            newErrors[idx] = 'something';
            this.setState({
              successes: newSuccesses,
              errors: newErrors
            });
          }.bind(this)
        );
    });
    Promise.all(gets).then(() => {
      this.setState({ searchInProgress: false });
    });
  }

  render() {
    const { usernames, successes, errors, searchInProgress } = this.state;
    const searchFields = usernames.map((un, idx) => {
      return (
        <SearchField
          key={idx}
          idx={idx}
          value={un}
          updateValue={this._updateUsername.bind(this)}
        />
      );
    });
    const profiles = successes.map((s, idx) => {
      if (s) {
        return <UserProfile key={idx} repoData={s} />;
      } else if (errors[idx]) {
        return (
          <div key={idx} className="error">
            something went wrong, probably username "{usernames[idx]}" was not
            found
          </div>
        );
      }
    });
    return (
      <div className="App">
        <div className="search">
          {searchFields}
          <input
            className="search-button"
            type="button"
            value="search!"
            onClick={this._doSearch.bind(this)}
          />
          <img
            className="App-logo"
            src={logo}
            style={searchInProgress ? {} : { display: 'none' }}
          />
        </div>
        <div className="results">{profiles}</div>
      </div>
    );
  }
}

export default App;
