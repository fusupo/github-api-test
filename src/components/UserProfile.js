import React, { Component } from 'react';
import ReposList from './RepoList';

class UserProfile extends Component {
  render() {
    const { repoData } = this.props;
    if (repoData.length > 0) {
      const owner = repoData[0].owner;
      const {
        avatar_url,
        followers_url,
        following_url,
        gists_url,
        id,
        organizations_url,
        starred_url
      } = owner;
      return (
        <div className="user-profile">
          <div className="profile">
            <img className="avatar" src={avatar_url} />
            <div>{id}</div>
          </div>
          <ReposList repoData={repoData} />
        </div>
      );
    } else {
      return null;
    }
  }
}

export default UserProfile;
