import React, { Component } from 'react';
import '../App.css';

class SearchField extends Component {
  render() {
    const { idx, value, updateValue } = this.props;
    return (
      <div className="search-field">
        <input
          type="text"
          value={value}
          onChange={e => updateValue(idx, e.target.value)}
        />
      </div>
    );
  }
}

export default SearchField;
