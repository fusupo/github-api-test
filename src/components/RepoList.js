import React, { Component } from 'react';
import moment from 'moment';

class RepoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sortCriteria: 'updated_at'
    };
  }

  _onHeaderItemClick(e) {
    this.setState({ sortCriteria: e.target.classList[0] });
  }

  render() {
    const { repoData } = this.props;
    const { sortCriteria } = this.state;
    const newRepoData = repoData.slice(0);

    newRepoData.sort((a, b) => {
      const comp_a = a[sortCriteria];
      const comp_b = b[sortCriteria];
      if (moment(comp_a).isValid()) {
        const mom_a = moment(comp_a);
        const mom_b = moment(comp_b);
        if (mom_a.isSame(mom_b)) {
          return 0;
        } else if (mom_a.isBefore(mom_b)) {
          return 1;
        } else {
          return -1;
        }
      } else if (!isNaN(parseInt(comp_a))) {
        return parseInt(comp_b) - parseInt(comp_a);
      } else {
        return comp_a.localeCompare(comp_b);
      }
    });

    const repos = newRepoData.map((r, idx) => {
      return (
        <div className="repo-item" key={idx}>
          <span className="size">{r.size}</span>
          <span className="name">
            {r.name}
            {r.fork === true ? '*' : ''}
          </span>
          <span className="created_at"> {moment(r.created_at).fromNow()}</span>
          <span className="updated_at">{moment(r.updated_at).fromNow()}</span>
        </div>
      );
    });

    const headerItems = [
      'size',
      'name',
      'created_at',
      'updated_at'
    ].map((t, idx) => {
      let classNames = t;

      classNames = t === sortCriteria ? classNames + ' selected' : classNames;
      return (
        <span
          key={idx}
          className={classNames}
          onClick={this._onHeaderItemClick.bind(this)}>
          {t}
        </span>
      );
    });

    return (
      <div className="repos-list">
        <div className="header">{headerItems}</div>
        <div className="repos">{repos}</div>
      </div>
    );
  }
}

export default RepoList;
